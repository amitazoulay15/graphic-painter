#include "Circle.h"

Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) :
	Shape(type, name)
{
	this->center = center;
	this->radius = radius;

}

Circle::~Circle()
{

}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

const Point& Circle::getCenter() const
{
	return this->center;
}

double Circle::getRadius() const
{
	return this->radius;
}

double Circle::getArea() const
{
	double area_of_circle = 0;
	area_of_circle = std::pow(getRadius(), 2);
	area_of_circle *= PI;
	return area_of_circle;
}


double Circle::getPerimeter() const
{
	double perimeter_of_circle = 0;
	perimeter_of_circle = 2 * PI * getRadius();
	return perimeter_of_circle;
}

void Circle::move(const Point& other)
{
	this->center = other;
}




