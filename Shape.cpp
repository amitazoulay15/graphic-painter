#include "Shape.h"
#include <iostream>
using namespace std;

Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::~Shape()
{
}

std::string Shape::getName() const
{
	return this->_name;
}

std::string Shape::getType() const
{
	return this->_type;
}

void Shape::printDetails() const
{
	std::cout << this->getName() << "		" << this->getType() << "	" << this->getArea() << "	" << this->getPerimeter() << std::endl;
}
