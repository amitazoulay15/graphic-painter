#pragma once
#include <cmath>

class Point
{
public:
	Point(double x, double y);
	Point(const Point& other);
	/*constractor*/
	Point(); //because to fix "not found default constractor" error

	/*destractor*/
	virtual ~Point();
	
	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	/*this function return the x of point
	input:none
	return: x of point*/
	double getX() const;

	/*this function return the y of point
	input:none
	return: y of point*/
	double getY() const;

	/*this function return the distance between 2 points
	input:other
	type:point
	return: distance*/
	double distance(const Point& other) const;

private:
	double x;
	double y;
};