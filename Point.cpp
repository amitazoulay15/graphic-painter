#include "Point.h"

Point::Point(double x, double y)
{
	this->x = x;
	this->y = y;
}

Point::Point(const Point& other) :
	Point(other.getX(), other.getY())
{
}

Point::Point() //because to fix "not found default constractor" error
{
}

Point::~Point()
{
}

double Point::getX() const
{
	return this->x;
}
double Point::getY() const
{
	return this->y;
}

Point Point::operator+(const Point& other) const
{
	double sum_of_x = 0, sum_of_y = 0;
	
	sum_of_x = this->getX() + other.getX();
	sum_of_y = this->getY() + other.getY();
	
	Point point(sum_of_x , sum_of_y);
	
	return point;
}

Point & Point::operator+=(const Point& other) 
{
	this->x += other.x;
	this->y += other.y;
	
	return *this;
}
double Point::distance(const Point& other) const
{
	double distance = 0;
	
	distance = std::pow(other.getX() - this->getX(), 2);
	distance += std::pow(other.getY() - this->getY(), 2);
	distance = std::sqrt(distance);
	
	return distance;
}