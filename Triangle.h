#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	/*constractor*/
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	

	virtual double getArea() const;
	virtual double getPerimeter() const;
	
	virtual void move(const Point& other);

	void draw(const Canvas& canvas);
	void clearDraw(const Canvas& canvas);

};