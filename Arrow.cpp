#include "Arrow.h"
#define AREA_OF_ARROW 0 //area of arrow always 0


Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) :
	Shape(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	
}

Arrow::~Arrow()
{
}

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}

void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

double Arrow::getArea() const
{
	return AREA_OF_ARROW;
}

double Arrow::getPerimeter() const
{
	double distance = 0;
	
	distance = this->_points[0].distance(this->_points[1]);
	
	return distance;
}

void Arrow::move(const Point& other)
{
	this->_points[0] = other;
	this->_points[1] += other;
}

