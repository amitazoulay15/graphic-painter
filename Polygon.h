#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : public Shape
{
public:
	/*constractor*/
	Polygon(const std::string& type, const std::string& name);

	/*destractor*/
	virtual ~Polygon();
	

	virtual void draw(const Canvas& canvas) = 0;
	virtual void clearDraw(const Canvas& canvas) = 0;

	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	

	virtual void move(const Point& other) = 0;

protected:
	std::vector<Point> _points;
};