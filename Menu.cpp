#include "Menu.h"

Menu::Menu() 
{
}

Menu::~Menu()
{
}

int Menu::printMenu()
{
		int option = 0;
	std::cout << "Enter 0 to add a new shape.\n"
		<< "Enter 1 to modify or get information from a current shape.\n"
		<< "Enter 2 to delete all of the shapes.\n"
		<< "Enter 3 to exit.\n"
		<< std::endl;
	std::cin >> option;
	

	if (option == 0)
	{
		print_menu_for_adding_a_shape();
	}
	else if (option == 1)
	{
		option_1(_shapeNames);
	}
	else if (option == 2)
	{
		_shapeNames.clear();
	}
	return option;
}
void Menu::print_menu_for_adding_a_shape()
{
	std::string menu_for_option = "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle.\n";
	int option_for_adding_a_shape = 0;

		std::cout << menu_for_option;
		std::cin >> option_for_adding_a_shape;


		switch (option_for_adding_a_shape)
		{
		case 0:
			circle();
			break;
		case 1:
			arrow();
			break;
		case 2:
			traingle();
			break;
		case 3:
			rectangle();
			break;
		}

	
}

void Menu::circle()
{
	double x = 0, y = 0, radius = 0;

	std::string name_of_shape;
	std::cout << "Please enter X: " << std::endl;
	std::cin >> x;

	std::cout << "Please enter Y: " << std::endl;
	std::cin >> y;

	do
	{
		std::cout << "Please enter radius: " << std::endl;
		std::cin >> radius;
	} while (radius <= 0);


	std::cout << "Please enter name of the shape: " << std::endl;
	std::cin >> name_of_shape;

	Point point(x, y);

	Circle * c = new Circle(point, radius, "circle", name_of_shape);

	_shapeNames.push_back(c);

	this->_shapeNames.back()->draw(this->_canvas);

}

void Menu::arrow()
{
	double x1 = 0, y1 = 0, x2 = 0, y2 = 0;

	std::string name_of_arrow;
	std::cout << "Please enter X of point number: 1 " << std::endl;
	std::cin >> x1;

	std::cout << "Please enter Y of point number: 1 " << std::endl;
	std::cin >> y1;

	std::cout << "Please enter X of point number: 2 " << std::endl;
	std::cin >> x2;

	std::cout << "Please enter Y of point number: 2 " << std::endl;
	std::cin >> y2;


	std::cout << "Please enter name of the shape: " << std::endl;
	std::cin >> name_of_arrow;

	Point point1(x1, y1);
	Point point2(x2, y2);

	Arrow *a = new Arrow(point1, point2, "arrow", name_of_arrow);

	_shapeNames.push_back(a);

	this->_shapeNames.back()->draw(this->_canvas);

}

void Menu::traingle()
{
	double x1 = 0, y1 = 0, x2 = 0, y2 = 0, x3 = 0, y3 = 0;

	std::string name_of_triangle;
	std::cout << "Please enter X of point number: 1 " << std::endl;
	std::cin >> x1;

	std::cout << "Please enter Y of point number: 1 " << std::endl;
	std::cin >> y1;

	std::cout << "Please enter X of point number: 2 " << std::endl;
	std::cin >> x2;

	std::cout << "Please enter Y of point number: 2 " << std::endl;
	std::cin >> y2;

	std::cout << "Please enter X of point number: 3 " << std::endl;
	std::cin >> x3;

	std::cout << "Please enter Y of point number: 3 " << std::endl;
	std::cin >> y3;


	std::cout << "Please enter name of the shape: " << std::endl;
	std::cin >> name_of_triangle;

	Point point1(x1, y1);
	Point point2(x2, y2);
	Point point3(x3, y3);

	Triangle * t = new Triangle (point1, point2, point3, "triangle", name_of_triangle);

	_shapeNames.push_back(t);

	this->_shapeNames.back()->draw(this->_canvas);

}

void Menu::rectangle()
{
	double x = 0, y = 0, lenght = 0, width = 0;

	std::string name_of_rectangle;
	std::cout << "Please enter X of the to left corner " << std::endl;
	std::cin >> x;

	std::cout << "Please enter Y of the to left corner " << std::endl;
	std::cin >> y;

	std::cout << "Please enter the lenght of the shape " << std::endl;
	std::cin >> lenght;

	std::cout << "Please enter width of the shape " << std::endl;
	std::cin >> width;


	std::cout << "Please enter name of the shape: " << std::endl;
	std::cin >> name_of_rectangle;

	Point point(x, y);
	

	myShapes::Rectangle * r = new myShapes::Rectangle(point, lenght, width, "rectangle", name_of_rectangle);

	_shapeNames.push_back(r);

	this->_shapeNames.back()->draw(this->_canvas);

}

void Menu::option_1(std::vector<Shape*>  shapeNames) const
{
	double x = 0, y = 0;
	int choice_of_shape = 0, choice_of_option = 0;
	std::string type_of_shape, name_of_shape;
	for (int i = 0; i < shapeNames.size(); i++)
	{
		std::cout << "Enter " << i << "for" << shapeNames[i]->getName() << "(" << shapeNames[i]->getType() << ")\n";
	}

	std::cin >> choice_of_shape;



	std::cout << "Enter 0 to move the shape\n"
		<< "Enter 1 to get its details.\n"
		<< "Enter 2 to remove the shape.\n";

	std::cin >> choice_of_option;

	if (choice_of_option == 0)
	{
		std::cout << "Please enter the X moving scale: ";
		std::cin >> x;
		std::cout << "Please enter the Y moving scale: ";
		std::cin >> y;
		Point point(x, y);
		shapeNames[choice_of_shape]->move(point);

	}

	else if (choice_of_option == 1)
	{
		shapeNames[choice_of_shape]->printDetails();
	}

	else if (choice_of_option == 2)
	{
		shapeNames[choice_of_shape]->clearDraw(this->_canvas);
	}
}

