#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);

}

Triangle::~Triangle()
{

}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

double Triangle::getArea() const
{
	double area_of_traingle = 0;

	area_of_traingle = std::abs(_points[0].getX() - _points[2].getX());
	area_of_traingle *= _points[1].getY() - _points[0].getY();
	area_of_traingle /= 2;
	
	return area_of_traingle;
}

double Triangle::getPerimeter() const
{
	double perimeter_of_traingle = 0;
	perimeter_of_traingle = std::abs(_points[0].getX() - _points[2].getX());
	perimeter_of_traingle += _points[0].distance(_points[1]) + _points[2].distance(_points[1]);
	return  perimeter_of_traingle;
}

void Triangle::move(const Point& other)
{
	this->_points[0] = other;
	this->_points[1] += other;
	this->_points[2] += other;
}
