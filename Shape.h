#pragma once
#include "Point.h"
#include "Canvas.h"
#include <string>

class Shape
{
public:
	/*constractor*/
	Shape(const std::string& name, const std::string& type);

	/*desstractor*/
	virtual ~Shape();

	/*this function return the area of the shape
	input:none
	return: the area of the shape*/
	virtual double getArea() const = 0;

	/*this function return the perimeter of the shape
	input:none
	return: the perimeter of the shape*/
	virtual double getPerimeter() const = 0;

	/*this function draw the shape with the help of canvas
	input:canvas
	type:canvas
	return: none*/
	virtual void draw(const Canvas& canvas) = 0;
	
	/*this function  add the Point to all the points of shape by "other"
	input:other
	type:point
	return :none*/
	virtual void move(const Point& other) = 0; 
	
	/*this function print the details of the shape
	input:none
	return :none*/
	void printDetails() const;
	
	/*this functionreturn the type of the shape
	input:none
	return _type:type*/
	std::string getType() const;
	
	/*this function return the name of the shape
	input:none
	return _name:name*/
	std::string getName() const;

	/*this function clear  draw
	input:canvas
	type:canvas
	return :void*/
	virtual void clearDraw(const Canvas& canvas) = 0;

private:
	std::string _name;
	std::string _type;
};