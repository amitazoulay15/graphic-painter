#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) :
	Polygon(type, name)
{
	Point b(a.getX() + width, a.getY() + length);
	_points.push_back(a);
	_points.push_back(b);
	
}

myShapes::Rectangle::~Rectangle()
{

}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

double myShapes::Rectangle::getArea() const
{
	double area_of_rectangle = 0;
	area_of_rectangle = this->_points[1].getX() - this->_points[0].getX();
	area_of_rectangle *= this->_points[1].getY() - this->_points[0].getY();
	return area_of_rectangle;
}

double myShapes::Rectangle::getPerimeter() const
{
	double perimeter_of_rectangle = 0;
	perimeter_of_rectangle = ((this->_points[1].getX() - this->_points[0].getX()) * 2) + ((this->_points[1].getY() - this->_points[0].getY()) * 2);
	return perimeter_of_rectangle;
}

void myShapes::Rectangle::move(const Point& other)
{
	this->_points[0] = other;
	this->_points[1] += other;
}

