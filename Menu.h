#pragma 

#include "Shape.h"
#include "Arrow.h"
#include "Circle.h"
#include "Polygon.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "Canvas.h"
#include <vector>
#include <iostream>



class Menu
{
public:
	/*constractor*/
	Menu();
	/*destractor*/
	~Menu();
	
	// more functions..
	int printMenu();
	void print_menu_for_adding_a_shape();

	/*this function add circle
	input:none
	return: none*/
	void circle();

	/*this function add arrow
	input:none
	return: none*/
	void arrow();

	/*this function add traingle
	input:none
	return: none*/
	void traingle();

	/*this function add rectangle
	input:none
	return: none*/
	void rectangle();

	/*This function is responsible for the operation of Option 1 on the menu
	input:shapeNames
	type: std::vector<Shape*>
	return:none*/
	void option_1(std::vector<Shape*> shapeNames) const;

private: 
	Canvas _canvas;
	std::vector<Shape*> _shapeNames;
	
};

